#!/bin/bash

echo "Se copiará o actualizará la plantilla."

sudo mkdir "/usr/share/texmf/tex/latex/plantilla-borisulianov"
sudo cp --update bu-libro-14x21.sty "/usr/share/texmf/tex/latex/plantilla-borisulianov/"
sudo cp --update bu-paper-a4.sty "/usr/share/texmf/tex/latex/plantilla-borisulianov/"
cd "/usr/share/texmf/tex/latex/plantilla-borisulianov"
sudo mktexlsr
sudo texhash

sudo apt-get install texlive-lang-spanish

tlmgr install utf8 setspace lipsum todonotes indentfirst emptypage titlesec stackengine graphicx pgfornament afterpage endnotes comment pdfpages tikz xkeyval

echo "¡Plantilla instalada!"