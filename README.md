# plantilla-libros-latex
Una plantilla para escribir libros en LaTeX.

Esta plantilla permite obtener libros de un tamaño normalizado de 21 cm por 14 cm; algo bastante común en la industria editorial. También contiene un comando para crear asterismos y florones, páginas en blanco, comentarios y notas de corrección.

## Documentación

* [Instalación](https://bitbucket.org/editorialborisulianov/plantilla-borisulianov/wiki/Instalaci%C3%B3n)
* [Uso](https://bitbucket.org/editorialborisulianov/plantilla-borisulianov/wiki/Uso)
* [Licencia](https://bitbucket.org/editorialborisulianov/plantilla-borisulianov/wiki/Licencia)
